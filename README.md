# mdbook Pages for MOKIT Doc

## Usage
```
tar xzvf mdbook-toc*.tar.gz
tar xzvf mdbook-v*.tar.gz
# modify md in src
./mdbook build
./mdbook serve
# open your browser to visit http://localhost:3000
```

## Features

### mdbook-toc

This template comes with `mdbook-toc`. That means you can insert `<!-- toc -->` into your pages to generate a table of contents.

<!--
### kdb-Element Style

A custom.css file for styled `<kdb>` elements to display keyboard inputs like <kbd>Ctrl</kbd> + <kbd>C</kbd> from the [MDN web docs](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/kbd).
-->

### Theme

Use Nord Theme from [gbrlsnchs/mdBook-nord-template](https://github.com/gbrlsnchs/mdBook-nord-template).

## Links

More Information:

- [https://yethiel.gitlab.io/post/mdbook-with-gitlab-ci-cd/](https://yethiel.gitlab.io/post/mdbook-with-gitlab-ci-cd/)
- [https://gitlab.com/yethiel/pages-mdbook](https://gitlab.com/yethiel/pages-mdbook)


